export { };
const express = require('express')
const booking = express()
const connection = require('../../models/booking.model.js')
const connectionUser = require('../../models/user.model.js')
const mongoose = require('mongoose')
const bookingModel = mongoose.model('bookings')
const userModel = mongoose.model('users')
const jwtToken = require('../UserAuth/jwtToken')
const adminjwtToken = require('../Admin/ad_jwtToken')
const ticketImport = require('../../models/ticket.model.js')
const ticketModel = mongoose.model('tickets')
const ObjectId = mongoose.Types.ObjectId

const bodyParser = require('body-parser')

booking.use(bodyParser.urlencoded({
  extended: false
}))

booking.use(bodyParser.json())

// Get userDetails
booking.get('/:id', adminjwtToken, async (req: any, res: any) => {
  await bookingModel.find({ ticketId: req.params.id, activeCheck: true }, (err: any, doc: any) => {
    if (!err) {
      if (doc.length === 0) {
        return res.status(404).send({
          auth: true,
          msg: 'Ticket Id is Invalid or Ticket is Open'
        })
      } else {
        if (doc.length > 1) {
          return res.status(404).send({
            auth: true,
            msg: 'Some error occured'
          })
        } else {
          const userId = (doc[0].userId).valueOf()
          userModel.find({ _id: userId }, (err: any, result: any) => {
            if (!err) {
              if (result.length === 0) {
                return res.status(404).send({
                  auth: true,
                  msg: 'Some error occured'
                })
              } else {
                if (result.length > 1) {
                  return res.status(404).send({
                    auth: true,
                    msg: 'Some error occured'
                  })
                } else {
                  const users = []
                  for (let i = 0; i < result.length; i++) {
                    const data = {
                      userName: result[i].userName,
                      userEmail: result[i].userEmail,
                      phoneNumber: result[i].phoneNumber,
                      userId: result[i]._id
                    }
                    users.push(data)
                  }
                  const d = {
                    users: users
                  }

                  return res.status(200).send({
                    auth: true,
                    data: d
                  })
                }
              }
            } else {
              return res.status(500).send({
                auth: true,
                msg: 'DB error'
              })
            }
          })
        }
      }
    } else {
      return res.status(500).send({
        auth: true,
        msg: 'DB error'
      })
    }
  })
})

booking.post('/add', async (req: any, res: any) => {
  if (!req.body.ticketId || !req.body.userId || !req.body.busId) {
    return res.status(400).send({
      msg: 'Bad payload'
    })
  }
  await ticketModel.find({ _id: req.body.ticketId }, (err: any, data: any) => {
    if (err) {
      return res.status(400).send({
        msg: 'DB error'
      })
    } else {
      // console.log(data);
      if (data[0].status !== 'CLOSED' && data.length === 1) {
        const newBooking = new bookingModel()
        newBooking.ticketId = data[0]._id
        newBooking.userId = req.body.userId
        newBooking.busId = req.body.busId
        newBooking.activeCheck = true
        newBooking.save((err: any, doc: any) => {
          if (!err) {
            ticketModel.updateOne({ _id: data[0]._id }, { status: 'CLOSED' }, (err: any, doc: any) => {
              if (!err) {
                return res.status(200).send({
                  msg: 'Seat is Booked Successfully and ticket updated'
                })
              } else {
                return res.status(404).send({
                  msg: 'Ticket not updated'
                })
              }
            })
          } else {
            return res.status(404).send({
              msg: 'Seat not booked!! Retry!!'
            })
          }
        })
      } else {
        return res.status(400).send({
          msg: 'Selected Ticket is already Booked'
        })
      }
    }
  })
})

// Get Bookings via userId
booking.get('/mybookings/:userid', jwtToken, async (req: any, res: any) => {
  await bookingModel.aggregate([
    { $match: { activeCheck: true, userId: new ObjectId(req.params.userid) } },
    { $lookup: { from: 'buses', localField: 'busId', foreignField: '_id', as: 'busDetails' } },
    { $lookup: { from: 'tickets', localField: 'ticketId', foreignField: '_id', as: 'ticketDetails' } }]
    , (err: any, doc: any) => {
      if (!err && doc !== null) {
        if (doc.length === 0) {
          return res.status(200).send({
            auth: true,
            msg: 'No Active Bookings'
          })
        } else {
          const bookings = []
          for (let i = 0; i < doc.length; i++) {
            const data = {
              bookingId: doc[i]._id,
              busId: doc[i].busDetails[0]._id,
              busName: doc[i].busDetails[0].busName,
              seatNo: doc[i].ticketDetails[0].seatNo
            }
            bookings.push(data)
          }
          const d = {
            bookings: bookings
          }

          return res.status(200).send({
            auth: true,
            data: d
          })
        }
      } else {
        return res.status(500).send({
          auth: true,
          msg: 'DB error'
        })
      }
    })
})

// Cancel a Booking via Booking ID
booking.put('/cancel/:id', jwtToken, async (req: any, res: any) => {
  await bookingModel.findOneAndUpdate({ _id: req.params.id, activeCheck: true }, { $set: { activeCheck: false } }, { new: true }, async (bookingErr: any, bookingDoc: any) => {
    if (!bookingErr && bookingDoc !== null) {
      await ticketModel.findOneAndUpdate({ _id: bookingDoc.ticketId, status: 'CLOSED' }, { $set: { status: 'OPEN' } }, async (ticketErr: any, ticketDoc: any) => {
        if (!ticketErr && ticketDoc !== null) {
          return res.status(200).send({
            auth: true,
            msg: 'Both Bookings and tickets reset is done'
          })
        } else {
          return res.status(404).send({
            auth: true,
            msg: 'Only booking reset is done and had ticket error'
          })
        }
      })
    } else {
      return res.status(500).send({
        auth: true,
        msg: 'Reset unsuccessful'
      })
    }
  })
})

module.exports = booking

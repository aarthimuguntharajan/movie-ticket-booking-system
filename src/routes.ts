import { Router } from "express"
import { get, getTodos, addTodo, updateTodo, deleteTodo } from "./controllers/todo"

const router: Router = Router()

router.get("/", get)
router.get("/todos", getTodos)

router.post("/add-todo", addTodo)

router.put("/edit-todo/:id", updateTodo)

router.delete("/delete-todo/:id", deleteTodo)

export default router
// import { Express, Request, Response } from "express";
// export default function (app: Express) {
//   app.get("/", (req: Request, res: Response) => res.send('Welcome to typescript project'))
// }
// const router = express()
// const cors = require('cors')
// router.use(cors())

// router.get('/harish', (req: express.Request, res: express.Response) => {
//   res.send('Welcome to Bus ticket Booking(2020)')
// })

// router.use('/auth', require('./UserAuth/authController'))

// router.use('/show', require('./Bus/busController'))

// router.use('/admin', require('./Admin/adminController'))
// router.use('/ticket', require('./Ticket/ticketController'))
// router.use('/booking', require('./Booking/bookingController'))

// // app.use('/admin', require('./admin/adminController'));

// // app.use('/user', require('./user/userController'));
// module.exports = router

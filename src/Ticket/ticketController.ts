export { };
const express = require('express')
const ticket = express()
const bodyParser = require('body-parser')

const dotenv = require('dotenv')
dotenv.config()
const jwtToken = require('../UserAuth/jwtToken')

const mongoose = require('mongoose')

const connection = require('../../models/ticket.model.js')
const ticketModel = mongoose.model('tickets')

ticket.use(bodyParser.urlencoded({
  extended: false
}))
ticket.use(bodyParser.json())

ticket.get('/', (req: any, res: any) => {
  res.send('Ticket Details')
})

// Get Open Tickets
ticket.get('/open/:id', jwtToken, async (req: any, res: any) => {
  await ticketModel.find({ status: 'OPEN', busId: req.params.id }, (err: any, doc: any) => {
    if (!err) {
      if (doc.length === 0) {
        return res.status(200).send({
          auth: true,
          msg: 'No Open tickets'
        })
      } else {
        const tickets = []
        for (let i = 0; i < doc.length; i++) {
          const data = {
            ticketId: doc[i]._id,
            busId: doc[i].busId,
            seatNo: doc[i].seatNo,
            status: doc[i].status
          }
          tickets.push(data)
        }
        const d = {
          tickets: tickets
        }

        return res.status(200).send({
          auth: true,
          data: d
        })
      }
    } else {
      return res.status(500).send({
        auth: true,
        msg: 'DB error'
      })
    }
  })
})

// Get Closed Tickets
ticket.get('/closed/:id', jwtToken, async (req: any, res: any) => {
  await ticketModel.find({ status: 'CLOSED', busId: req.params.id }, (err: any, doc: any) => {
    if (!err) {
      if (doc.length === 0) {
        return res.status(200).send({
          auth: true,
          msg: 'No Closed tickets'
        })
      } else {
        const tickets = []
        for (let i = 0; i < doc.length; i++) {
          const data = {
            ticketId: doc[i]._id,
            busId: doc[i].busId,
            seatNo: doc[i].seatNo,
            status: doc[i].status
          }
          tickets.push(data)
        }
        const d = {
          tickets: tickets
        }
        // console.log(String(Date()));
        return res.status(200).send({
          auth: true,
          data: d
        })
      }
    } else {
      return res.status(500).send({
        auth: true,
        msg: 'DB error'
      })
    }
  })
})

// get Ticket status
ticket.get('/:id', jwtToken, async (req: any, res: any) => {
  await ticketModel.find({ _id: req.params.id }, (err: any, doc: any) => {
    if (!err) {
      if (doc.length === 0) {
        return res.status(404).send({
          auth: true,
          msg: 'Invalid Ticket Id'
        })
      } else {
        if (doc.length > 1) {
          return res.status(500).send({
            auth: true,
            msg: 'An error occured'
          })
        } else {
          const tickets = []
          for (let i = 0; i < doc.length; i++) {
            const data = {
              seatNo: doc[i].seatNo,
              status: doc[i].status
            }
            tickets.push(data)
          }
          const d = {
            tickets: tickets
          }
          return res.status(200).send({
            auth: true,
            data: d
          })
        }
      }
    } else {
      return res.status(500).send({
        auth: true,
        msg: 'DB error'
      })
    }
  })
})

module.exports = ticket

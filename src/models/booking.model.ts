export { };
const mongoose = require('mongoose')

const bookingSchema = new mongoose.Schema({
  busId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'buses',
    required: 'Required'
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'users',
    required: 'Required'
  },
  ticketId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'tickets',
    required: 'Required'
  },
  activeCheck: {
    type: Boolean,
    required: 'Required'
  }
},
  {
    timestamps: true
  })

mongoose.model('bookings', bookingSchema)

export { };
const mongoose = require('mongoose')

const adminSchema = new mongoose.Schema({
  adminName: {
    type: String,
    required: 'Required'
  },
  adminEmail: {
    type: String,
    required: 'Required'
  },
  adminPassword: {
    type: String,
    required: 'Required'
  }
})

mongoose.model('admins', adminSchema)

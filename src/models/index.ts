import * as Mongoose from "mongoose";
let database: Mongoose.Connection;
export const connect = () => {
  // add your own uri below
  const uri = "mongodb+srv://dbUser:dbUser123456@cluster0.esg4z.mongodb.net/sample_airbnb?retryWrites=true&w=majority";
  if (database) {
    return;
  }
  Mongoose.connect(uri, {
    useNewUrlParser: true,
    useFindAndModify: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  });
  database = Mongoose.connection;
  database.once("open", async () => {
    console.log("Connected to database");
  });
  database.on("error", () => {
    console.log("Error connecting to database");
  });
};
export const disconnect = () => {
  if (!database) {
    return;
  }
  Mongoose.disconnect();
};
// const userModel = require('./user.model');
// const busModel = require('./bus.model');
// const adminModel = require('./admin.model');
// const ticketModel = require('./ticket.model');

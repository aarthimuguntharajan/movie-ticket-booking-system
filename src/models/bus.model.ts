export { };
const mongoose = require('mongoose')

const busSchema = new mongoose.Schema({
  busName: {
    type: String,
    required: 'Required'
  },
  busTiming: {
    type: String,
    required: 'Required'
  },
  busFromTo: {
    type: String,
    required: 'Required'
  },
  noOfTickets: {
    type: Number,
    required: 'Required'
  },
  createdAt: {
    type: Date,
    required: 'Required',
    default: Date.now
  },
  activeCheck: {
    type: Boolean,
    required: 'Required'
  }
})

mongoose.model('buses', busSchema)

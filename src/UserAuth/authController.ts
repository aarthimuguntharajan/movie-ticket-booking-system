export { };
const express = require('express')
const user = express()
const bodyParser = require('body-parser')

const dotenv = require('dotenv')
dotenv.config()

const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')

const mongoose = require('mongoose')
const validator = require('email-validator')

const connection = require('../../models/user.model.js')
const userModel = mongoose.model('users')

user.use(bodyParser.urlencoded({
  extended: false
}))
user.use(bodyParser.json())

user.get('/', (req: any, res: any) => {
  res.send('User Details')
})

user.post('/register', async (req: any, res: any) => {
  if (!req.body.name || !req.body.email || !req.body.password || !req.body.phone) {
    return res.status(400).send({
      msg: 'Bad payload'
    })
  }

  if (!validator.validate(req.body.email)) {
    return res.status(404).send({
      msg: 'Email badly formatted'
    })
  }
  // console.log(req.body.password);
  const pwd = await bcrypt.hashSync(req.body.password, 8)
  const userReg = new userModel()
  userReg.userName = req.body.name
  userReg.userEmail = req.body.email
  userReg.userPassword = pwd
  userReg.phoneNumber = req.body.phone
  userReg.createdAt = new Date()
  await userReg.save((err: any, doc: any) => {
    if (!err) {
      return res.status(200).send({
        msg: req.body.name + ' your details registered succesfully'
      })
    } else {
      return res.status(404).send({
        msg: 'User Details not registered, try again'
      })
    }
  })
})

user.post('/login', async (req: any, res: any) => {
  if (!req.body.email || !req.body.password) {
    return res.status(400).send({
      auth: false,
      msg: 'Bad payload'
    })
  }
  // console.log(req.body.email,req.body.password);
  await userModel.find({ userEmail: req.body.email }, (err: any, doc: any) => {
    if (!err) {
      if (doc.length == 0) {
        return res.status(404).send({
          auth: false,
          msg: 'Invalid Email'
        })
      } else {
        const encryptedPassword = doc[0].userPassword
        // console.log(doc[0].userPassword, req.body.password);
        const passwordIsValid = bcrypt.compareSync(req.body.password, encryptedPassword)
        if (!passwordIsValid) {
          return res.status(404).send({
            auth: false,
            msg: 'Invalid Password'
          })
        } else {
          const token = jwt.sign({
            id: doc[0]._id,
            email: doc[0].userEmail
          }, process.env.jwtSecret, {
            expiresIn: 86400000 * 15 // 15 days
          })

          const data = {
            id: doc[0]._id,
            name: doc[0].userName,
            email: doc[0].userEmail,
            phone: doc[0].phoneNumber
          }

          return res.status(200).send({
            auth: true,
            token: token,
            msg: 'Login success :)',
            data: data
          })
        }
      }
    } else {
      return res.status(500).send({
        auth: false,
        msg: 'DB error'
      })
    }
  })
})

module.exports = user

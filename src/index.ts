//import * as express from "express"
const express = require("express")
const mongoose = require('mongoose')
const bodyParser = require('body-parser');
//import * as mongoose from "mongoose"
import { connect } from './models/index'
const cors = require("cors")
import todoRoutes from "./routes"

const app = express()

const PORT: string | number = 5000
app.use(express.json())
app.use(express.urlencoded({
  extended: true
}));
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }));
//app.use(cors())
app.use(todoRoutes)

const uri: string = 'mongodb+srv://dbUser:dbUser123456@cluster0.esg4z.mongodb.net/sample_airbnb?retryWrites=true&w=majority'
const options = { useNewUrlParser: true, useUnifiedTopology: true }
mongoose.set("useFindAndModify", false)

mongoose
  .connect(uri, options)
  .then(() =>
    app.listen(PORT, () =>
      console.log(`App Server running on http://localhost:${PORT}`)
    )
  )

  .catch((error: any) => {
    throw error
  })


// import * as express from 'express'
// import routes from './src/routes';
// import { connect } from "./src/models/index";
// import { Express } from 'express';
// conimport { mongoose } from 'mongoose';
// st import { Express } from 'express';
// app = express()
// const port = process.env.PORT || 5555
// app.use(express.json())
// app.use(express.urlencoded({ extended: false }));
//const cors = require('cors')
//router.use(cors())


//router.use('/auth', require('./UserAuth/authController'))

//router.use('/show', require('./Bus/busController'))

//router.use('/admin', require('./Admin/adminController'))
//router.use('/ticket', require('./Ticket/ticketController'))
//router.use('/booking', require('./Booking/bookingController'))
//const connection =
//require('./models/index')


// app.listen(port, () => {
//   console.log('Ticket Booking API is listening on  http://localhost:' + port)
//   connect()
//   routes(app)
// })


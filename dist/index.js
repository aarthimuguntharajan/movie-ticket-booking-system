"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
//import * as express from "express"
var express = require("express");
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var cors = require("cors");
var routes_1 = __importDefault(require("./routes"));
var app = express();
var PORT = 5000;
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//app.use(cors())
app.use(routes_1.default);
var uri = 'mongodb+srv://dbUser:dbUser123456@cluster0.esg4z.mongodb.net/sample_airbnb?retryWrites=true&w=majority';
var options = { useNewUrlParser: true, useUnifiedTopology: true };
mongoose.set("useFindAndModify", false);
mongoose
    .connect(uri, options)
    .then(function () {
    return app.listen(PORT, function () {
        return console.log("App Server running on http://localhost:" + PORT);
    });
})
    .catch(function (error) {
    throw error;
});
// import * as express from 'express'
// import routes from './src/routes';
// import { connect } from "./src/models/index";
// import { Express } from 'express';
// conimport { mongoose } from 'mongoose';
// st import { Express } from 'express';
// app = express()
// const port = process.env.PORT || 5555
// app.use(express.json())
// app.use(express.urlencoded({ extended: false }));
//const cors = require('cors')
//router.use(cors())
//router.use('/auth', require('./UserAuth/authController'))
//router.use('/show', require('./Bus/busController'))
//router.use('/admin', require('./Admin/adminController'))
//router.use('/ticket', require('./Ticket/ticketController'))
//router.use('/booking', require('./Booking/bookingController'))
//const connection =
//require('./models/index')
// app.listen(port, () => {
//   console.log('Ticket Booking API is listening on  http://localhost:' + port)
//   connect()
//   routes(app)
// })

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var todo_1 = require("./controllers/todo");
var router = express_1.Router();
router.get("/", todo_1.get);
router.get("/todos", todo_1.getTodos);
router.post("/add-todo", todo_1.addTodo);
router.put("/edit-todo/:id", todo_1.updateTodo);
router.delete("/delete-todo/:id", todo_1.deleteTodo);
exports.default = router;
// import { Express, Request, Response } from "express";
// export default function (app: Express) {
//   app.get("/", (req: Request, res: Response) => res.send('Welcome to typescript project'))
// }
// const router = express()
// const cors = require('cors')
// router.use(cors())
// router.get('/harish', (req: express.Request, res: express.Response) => {
//   res.send('Welcome to Bus ticket Booking(2020)')
// })
// router.use('/auth', require('./UserAuth/authController'))
// router.use('/show', require('./Bus/busController'))
// router.use('/admin', require('./Admin/adminController'))
// router.use('/ticket', require('./Ticket/ticketController'))
// router.use('/booking', require('./Booking/bookingController'))
// // app.use('/admin', require('./admin/adminController'));
// // app.use('/user', require('./user/userController'));
// module.exports = router

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = require('mongoose');
var busSchema = new mongoose.Schema({
    busName: {
        type: String,
        required: 'Required'
    },
    busTiming: {
        type: String,
        required: 'Required'
    },
    busFromTo: {
        type: String,
        required: 'Required'
    },
    noOfTickets: {
        type: Number,
        required: 'Required'
    },
    createdAt: {
        type: Date,
        required: 'Required',
        default: Date.now
    },
    activeCheck: {
        type: Boolean,
        required: 'Required'
    }
});
mongoose.model('buses', busSchema);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = require('mongoose');
var adminSchema = new mongoose.Schema({
    adminName: {
        type: String,
        required: 'Required'
    },
    adminEmail: {
        type: String,
        required: 'Required'
    },
    adminPassword: {
        type: String,
        required: 'Required'
    }
});
mongoose.model('admins', adminSchema);

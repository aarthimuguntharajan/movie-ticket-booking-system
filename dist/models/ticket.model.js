"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = require('mongoose');
var ticketSchema = new mongoose.Schema({
    busId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Bus',
        required: 'Required'
    },
    seatNo: {
        type: String,
        required: 'Required'
    },
    status: {
        type: String,
        required: 'Required'
    }
});
mongoose.model('tickets', ticketSchema);

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var express = require('express');
var bus = express();
var bodyParser = require('body-parser');
//const dotenv = require('dotenv')
//dotenv.config()
var adminjwtToken = require('../Admin/ad_jwtToken');
var mongoose = require('mongoose');
// eslint-disable-next-line no-unused-vars
var connection = require('../../models/bus.model.js');
var busModel = mongoose.model('buses');
var connectionTicket = require('../../models/ticket.model.js');
var ticketModel = mongoose.model('tickets');
var connectionModel = require('../../models/booking.model.js');
var bookingModel = mongoose.model('bookings');
bus.use(bodyParser.urlencoded({
    extended: false
}));
bus.use(bodyParser.json());
bus.get('/', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, busModel.find(function (err, doc) {
                    if (!err) {
                        if (doc.length === 0) {
                            return res.status(404).send({
                                auth: true,
                                msg: 'No Data'
                            });
                        }
                        else {
                            var buses = [];
                            for (var i = 0; i < doc.length; i++) {
                                var data = {
                                    busId: doc[i]._id,
                                    busName: doc[i].busName,
                                    busTiming: doc[i].busTiming,
                                    busFromTo: doc[i].busFromTo,
                                    active: doc[i].activeCheck,
                                    noOfTickets: doc[i].noOfTickets
                                };
                                buses.push(data);
                            }
                            var d = {
                                Buses: buses
                            };
                            // console.log(String(Date()));
                            return res.status(200).send({
                                auth: true,
                                data: d
                            });
                        }
                    }
                    else {
                        return res.status(500).send({
                            auth: true,
                            msg: "Bus Details couldn't be fetched. Please try again"
                        });
                    }
                })];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
bus.post('/add', adminjwtToken, function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var busReg;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                if (!req.body.name || !req.body.timing || !req.body.fromTo || !req.body.noOfTickets) {
                    return [2 /*return*/, res.status(400).send({
                            msg: 'Bad payload'
                        })];
                }
                busReg = new busModel();
                busReg.busName = req.body.name;
                busReg.busTiming = req.body.timing;
                busReg.busFromTo = req.body.fromTo;
                busReg.noOfTickets = req.body.noOfTickets;
                busReg.activeCheck = true;
                busReg.createdAt = new Date();
                return [4 /*yield*/, busReg.save(function (busErr, busDoc) {
                        if (!busErr && busDoc !== null) {
                            var tickets = [];
                            for (var i = 1; i <= req.body.noOfTickets; i++) {
                                var newTicket = new ticketModel();
                                newTicket.busId = busDoc._id;
                                newTicket.seatNo = "S" + i;
                                newTicket.status = 'OPEN';
                                tickets.push(newTicket);
                            }
                            ticketModel.insertMany(tickets, function (ticketErr, ticketDoc) {
                                if (!ticketErr && ticketDoc !== null) {
                                    return res.status(200).send({
                                        msg: 'Bus and Tickets Added Successfully'
                                    });
                                }
                                else {
                                    return res.status(404).send({
                                        msg: 'Bus Added succesfully but Tickets Not Added'
                                    });
                                }
                            });
                        }
                        else {
                            return res.status(404).send({
                                msg: 'Bus not Added, try again'
                            });
                        }
                    })];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
bus.delete('/deletebus/:id', adminjwtToken, function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, busModel.deleteOne({ _id: req.params.id }, function (busErr, busDoc) { return __awaiter(void 0, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (!!busErr) return [3 /*break*/, 2];
                                return [4 /*yield*/, ticketModel.deleteMany({ busId: req.params.id }, function (ticketErr, ticketDoc) { return __awaiter(void 0, void 0, void 0, function () {
                                        return __generator(this, function (_a) {
                                            switch (_a.label) {
                                                case 0:
                                                    if (!!ticketErr) return [3 /*break*/, 2];
                                                    return [4 /*yield*/, bookingModel.updateMany({ busId: req.params.id, activeCheck: true }, { $set: { activeCheck: false } }, function (bookingErr, bookingDoc) { return __awaiter(void 0, void 0, void 0, function () {
                                                            return __generator(this, function (_a) {
                                                                if (!bookingErr) {
                                                                    return [2 /*return*/, res.status(200).send({
                                                                            auth: true,
                                                                            msg: 'Both Bus and the corresponding tickets removed successfully and Bookings set to inactive.'
                                                                        })];
                                                                }
                                                                else {
                                                                    return [2 /*return*/, res.status(404).send({
                                                                            auth: true,
                                                                            msg: 'Both Bus and the corresponding tickets removed successfully but tickets not set to inactive.'
                                                                        })];
                                                                }
                                                                return [2 /*return*/];
                                                            });
                                                        }); })];
                                                case 1:
                                                    _a.sent();
                                                    return [3 /*break*/, 3];
                                                case 2: return [2 /*return*/, res.status(404).send({
                                                        auth: true,
                                                        msg: 'Bus deleted but Tickets not deleted'
                                                    })];
                                                case 3: return [2 /*return*/];
                                            }
                                        });
                                    }); })];
                            case 1:
                                _a.sent();
                                return [3 /*break*/, 3];
                            case 2: return [2 /*return*/, res.status(500).send({
                                    auth: true,
                                    msg: 'Bus not deleted. Please try again'
                                })];
                            case 3: return [2 /*return*/];
                        }
                    });
                }); })];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
module.exports = bus;

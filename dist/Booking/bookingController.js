"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var express = require('express');
var booking = express();
var connection = require('../../models/booking.model.js');
var connectionUser = require('../../models/user.model.js');
var mongoose = require('mongoose');
var bookingModel = mongoose.model('bookings');
var userModel = mongoose.model('users');
var jwtToken = require('../UserAuth/jwtToken');
var adminjwtToken = require('../Admin/ad_jwtToken');
var ticketImport = require('../../models/ticket.model.js');
var ticketModel = mongoose.model('tickets');
var ObjectId = mongoose.Types.ObjectId;
var bodyParser = require('body-parser');
booking.use(bodyParser.urlencoded({
    extended: false
}));
booking.use(bodyParser.json());
// Get userDetails
booking.get('/:id', adminjwtToken, function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, bookingModel.find({ ticketId: req.params.id, activeCheck: true }, function (err, doc) {
                    if (!err) {
                        if (doc.length === 0) {
                            return res.status(404).send({
                                auth: true,
                                msg: 'Ticket Id is Invalid or Ticket is Open'
                            });
                        }
                        else {
                            if (doc.length > 1) {
                                return res.status(404).send({
                                    auth: true,
                                    msg: 'Some error occured'
                                });
                            }
                            else {
                                var userId = (doc[0].userId).valueOf();
                                userModel.find({ _id: userId }, function (err, result) {
                                    if (!err) {
                                        if (result.length === 0) {
                                            return res.status(404).send({
                                                auth: true,
                                                msg: 'Some error occured'
                                            });
                                        }
                                        else {
                                            if (result.length > 1) {
                                                return res.status(404).send({
                                                    auth: true,
                                                    msg: 'Some error occured'
                                                });
                                            }
                                            else {
                                                var users = [];
                                                for (var i = 0; i < result.length; i++) {
                                                    var data = {
                                                        userName: result[i].userName,
                                                        userEmail: result[i].userEmail,
                                                        phoneNumber: result[i].phoneNumber,
                                                        userId: result[i]._id
                                                    };
                                                    users.push(data);
                                                }
                                                var d = {
                                                    users: users
                                                };
                                                return res.status(200).send({
                                                    auth: true,
                                                    data: d
                                                });
                                            }
                                        }
                                    }
                                    else {
                                        return res.status(500).send({
                                            auth: true,
                                            msg: 'DB error'
                                        });
                                    }
                                });
                            }
                        }
                    }
                    else {
                        return res.status(500).send({
                            auth: true,
                            msg: 'DB error'
                        });
                    }
                })];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
booking.post('/add', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                if (!req.body.ticketId || !req.body.userId || !req.body.busId) {
                    return [2 /*return*/, res.status(400).send({
                            msg: 'Bad payload'
                        })];
                }
                return [4 /*yield*/, ticketModel.find({ _id: req.body.ticketId }, function (err, data) {
                        if (err) {
                            return res.status(400).send({
                                msg: 'DB error'
                            });
                        }
                        else {
                            // console.log(data);
                            if (data[0].status !== 'CLOSED' && data.length === 1) {
                                var newBooking = new bookingModel();
                                newBooking.ticketId = data[0]._id;
                                newBooking.userId = req.body.userId;
                                newBooking.busId = req.body.busId;
                                newBooking.activeCheck = true;
                                newBooking.save(function (err, doc) {
                                    if (!err) {
                                        ticketModel.updateOne({ _id: data[0]._id }, { status: 'CLOSED' }, function (err, doc) {
                                            if (!err) {
                                                return res.status(200).send({
                                                    msg: 'Seat is Booked Successfully and ticket updated'
                                                });
                                            }
                                            else {
                                                return res.status(404).send({
                                                    msg: 'Ticket not updated'
                                                });
                                            }
                                        });
                                    }
                                    else {
                                        return res.status(404).send({
                                            msg: 'Seat not booked!! Retry!!'
                                        });
                                    }
                                });
                            }
                            else {
                                return res.status(400).send({
                                    msg: 'Selected Ticket is already Booked'
                                });
                            }
                        }
                    })];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
// Get Bookings via userId
booking.get('/mybookings/:userid', jwtToken, function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, bookingModel.aggregate([
                    { $match: { activeCheck: true, userId: new ObjectId(req.params.userid) } },
                    { $lookup: { from: 'buses', localField: 'busId', foreignField: '_id', as: 'busDetails' } },
                    { $lookup: { from: 'tickets', localField: 'ticketId', foreignField: '_id', as: 'ticketDetails' } }
                ], function (err, doc) {
                    if (!err && doc !== null) {
                        if (doc.length === 0) {
                            return res.status(200).send({
                                auth: true,
                                msg: 'No Active Bookings'
                            });
                        }
                        else {
                            var bookings = [];
                            for (var i = 0; i < doc.length; i++) {
                                var data = {
                                    bookingId: doc[i]._id,
                                    busId: doc[i].busDetails[0]._id,
                                    busName: doc[i].busDetails[0].busName,
                                    seatNo: doc[i].ticketDetails[0].seatNo
                                };
                                bookings.push(data);
                            }
                            var d = {
                                bookings: bookings
                            };
                            return res.status(200).send({
                                auth: true,
                                data: d
                            });
                        }
                    }
                    else {
                        return res.status(500).send({
                            auth: true,
                            msg: 'DB error'
                        });
                    }
                })];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
// Cancel a Booking via Booking ID
booking.put('/cancel/:id', jwtToken, function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, bookingModel.findOneAndUpdate({ _id: req.params.id, activeCheck: true }, { $set: { activeCheck: false } }, { new: true }, function (bookingErr, bookingDoc) { return __awaiter(void 0, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (!(!bookingErr && bookingDoc !== null)) return [3 /*break*/, 2];
                                return [4 /*yield*/, ticketModel.findOneAndUpdate({ _id: bookingDoc.ticketId, status: 'CLOSED' }, { $set: { status: 'OPEN' } }, function (ticketErr, ticketDoc) { return __awaiter(void 0, void 0, void 0, function () {
                                        return __generator(this, function (_a) {
                                            if (!ticketErr && ticketDoc !== null) {
                                                return [2 /*return*/, res.status(200).send({
                                                        auth: true,
                                                        msg: 'Both Bookings and tickets reset is done'
                                                    })];
                                            }
                                            else {
                                                return [2 /*return*/, res.status(404).send({
                                                        auth: true,
                                                        msg: 'Only booking reset is done and had ticket error'
                                                    })];
                                            }
                                            return [2 /*return*/];
                                        });
                                    }); })];
                            case 1:
                                _a.sent();
                                return [3 /*break*/, 3];
                            case 2: return [2 /*return*/, res.status(500).send({
                                    auth: true,
                                    msg: 'Reset unsuccessful'
                                })];
                            case 3: return [2 /*return*/];
                        }
                    });
                }); })];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
module.exports = booking;

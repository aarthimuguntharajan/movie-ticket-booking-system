"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var express = require('express');
var user = express();
var bodyParser = require('body-parser');
var dotenv = require('dotenv');
dotenv.config();
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var mongoose = require('mongoose');
var validator = require('email-validator');
var connection = require('../../models/user.model.js');
var userModel = mongoose.model('users');
user.use(bodyParser.urlencoded({
    extended: false
}));
user.use(bodyParser.json());
user.get('/', function (req, res) {
    res.send('User Details');
});
user.post('/register', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var pwd, userReg;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                if (!req.body.name || !req.body.email || !req.body.password || !req.body.phone) {
                    return [2 /*return*/, res.status(400).send({
                            msg: 'Bad payload'
                        })];
                }
                if (!validator.validate(req.body.email)) {
                    return [2 /*return*/, res.status(404).send({
                            msg: 'Email badly formatted'
                        })];
                }
                return [4 /*yield*/, bcrypt.hashSync(req.body.password, 8)];
            case 1:
                pwd = _a.sent();
                userReg = new userModel();
                userReg.userName = req.body.name;
                userReg.userEmail = req.body.email;
                userReg.userPassword = pwd;
                userReg.phoneNumber = req.body.phone;
                userReg.createdAt = new Date();
                return [4 /*yield*/, userReg.save(function (err, doc) {
                        if (!err) {
                            return res.status(200).send({
                                msg: req.body.name + ' your details registered succesfully'
                            });
                        }
                        else {
                            return res.status(404).send({
                                msg: 'User Details not registered, try again'
                            });
                        }
                    })];
            case 2:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
user.post('/login', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                if (!req.body.email || !req.body.password) {
                    return [2 /*return*/, res.status(400).send({
                            auth: false,
                            msg: 'Bad payload'
                        })];
                }
                // console.log(req.body.email,req.body.password);
                return [4 /*yield*/, userModel.find({ userEmail: req.body.email }, function (err, doc) {
                        if (!err) {
                            if (doc.length == 0) {
                                return res.status(404).send({
                                    auth: false,
                                    msg: 'Invalid Email'
                                });
                            }
                            else {
                                var encryptedPassword = doc[0].userPassword;
                                // console.log(doc[0].userPassword, req.body.password);
                                var passwordIsValid = bcrypt.compareSync(req.body.password, encryptedPassword);
                                if (!passwordIsValid) {
                                    return res.status(404).send({
                                        auth: false,
                                        msg: 'Invalid Password'
                                    });
                                }
                                else {
                                    var token = jwt.sign({
                                        id: doc[0]._id,
                                        email: doc[0].userEmail
                                    }, process.env.jwtSecret, {
                                        expiresIn: 86400000 * 15 // 15 days
                                    });
                                    var data = {
                                        id: doc[0]._id,
                                        name: doc[0].userName,
                                        email: doc[0].userEmail,
                                        phone: doc[0].phoneNumber
                                    };
                                    return res.status(200).send({
                                        auth: true,
                                        token: token,
                                        msg: 'Login success :)',
                                        data: data
                                    });
                                }
                            }
                        }
                        else {
                            return res.status(500).send({
                                auth: false,
                                msg: 'DB error'
                            });
                        }
                    })];
            case 1:
                // console.log(req.body.email,req.body.password);
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
module.exports = user;

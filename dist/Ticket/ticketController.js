"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var express = require('express');
var ticket = express();
var bodyParser = require('body-parser');
var dotenv = require('dotenv');
dotenv.config();
var jwtToken = require('../UserAuth/jwtToken');
var mongoose = require('mongoose');
var connection = require('../../models/ticket.model.js');
var ticketModel = mongoose.model('tickets');
ticket.use(bodyParser.urlencoded({
    extended: false
}));
ticket.use(bodyParser.json());
ticket.get('/', function (req, res) {
    res.send('Ticket Details');
});
// Get Open Tickets
ticket.get('/open/:id', jwtToken, function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, ticketModel.find({ status: 'OPEN', busId: req.params.id }, function (err, doc) {
                    if (!err) {
                        if (doc.length === 0) {
                            return res.status(200).send({
                                auth: true,
                                msg: 'No Open tickets'
                            });
                        }
                        else {
                            var tickets = [];
                            for (var i = 0; i < doc.length; i++) {
                                var data = {
                                    ticketId: doc[i]._id,
                                    busId: doc[i].busId,
                                    seatNo: doc[i].seatNo,
                                    status: doc[i].status
                                };
                                tickets.push(data);
                            }
                            var d = {
                                tickets: tickets
                            };
                            return res.status(200).send({
                                auth: true,
                                data: d
                            });
                        }
                    }
                    else {
                        return res.status(500).send({
                            auth: true,
                            msg: 'DB error'
                        });
                    }
                })];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
// Get Closed Tickets
ticket.get('/closed/:id', jwtToken, function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, ticketModel.find({ status: 'CLOSED', busId: req.params.id }, function (err, doc) {
                    if (!err) {
                        if (doc.length === 0) {
                            return res.status(200).send({
                                auth: true,
                                msg: 'No Closed tickets'
                            });
                        }
                        else {
                            var tickets = [];
                            for (var i = 0; i < doc.length; i++) {
                                var data = {
                                    ticketId: doc[i]._id,
                                    busId: doc[i].busId,
                                    seatNo: doc[i].seatNo,
                                    status: doc[i].status
                                };
                                tickets.push(data);
                            }
                            var d = {
                                tickets: tickets
                            };
                            // console.log(String(Date()));
                            return res.status(200).send({
                                auth: true,
                                data: d
                            });
                        }
                    }
                    else {
                        return res.status(500).send({
                            auth: true,
                            msg: 'DB error'
                        });
                    }
                })];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
// get Ticket status
ticket.get('/:id', jwtToken, function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, ticketModel.find({ _id: req.params.id }, function (err, doc) {
                    if (!err) {
                        if (doc.length === 0) {
                            return res.status(404).send({
                                auth: true,
                                msg: 'Invalid Ticket Id'
                            });
                        }
                        else {
                            if (doc.length > 1) {
                                return res.status(500).send({
                                    auth: true,
                                    msg: 'An error occured'
                                });
                            }
                            else {
                                var tickets = [];
                                for (var i = 0; i < doc.length; i++) {
                                    var data = {
                                        seatNo: doc[i].seatNo,
                                        status: doc[i].status
                                    };
                                    tickets.push(data);
                                }
                                var d = {
                                    tickets: tickets
                                };
                                return res.status(200).send({
                                    auth: true,
                                    data: d
                                });
                            }
                        }
                    }
                    else {
                        return res.status(500).send({
                            auth: true,
                            msg: 'DB error'
                        });
                    }
                })];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
module.exports = ticket;
